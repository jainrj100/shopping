from django.shortcuts import render,redirect
from .forms import AddressForm
from django.utils.http import is_safe_url
from billing.models import BillingProfile
# Create your views here.
def checkout_address_create_view(request):
    form=AddressForm(request.POST or None)
    context={
        'form':form,
    }
    next_ = request.GET.get('next')
    next_post = request.POST.get('next')
    redirect_path = next_ or next_post
    if form.is_valid():
        instance=form.save(commit=False)
        billing_profile, billing_profile_created = BillingProfile.objects.new_or_get(request)
        if billing_profile is not None:
            addresss_type=request.POST.get('address_type','shipping')
            instance.billing_profile=billing_profile
            instance.address_type=addresss_type
            instance.save()
            request.session[addresss_type + '_address_id']=instance.id
            print(addresss_type + '_address_id')
        else:
            return redirect('checkout')
        if is_safe_url(redirect_path, request.get_host()):
            return redirect(redirect_path)
        else:
            return redirect('checkout')
    return redirect('checkout')