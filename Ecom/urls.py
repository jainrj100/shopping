"""Ecom URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include,re_path
from django.conf.urls.static import static
from django.conf import settings
from ecommerce import views
from accounts.views import login_page,register_page,logout_view
from carts.views import cart_home,cart_update,checkout_home,checkout_done
from addresses.views import checkout_address_create_view


urlpatterns = [
    path('admin/', admin.site.urls),
path('',views.home,name='homepage' ),
# re_path('^contact/$',views.contact,name='contact' ),
re_path('^cart/$',cart_home,name='cart' ),
re_path('^checkout/$',checkout_home,name='checkout'),
path('checkout/address/create',checkout_address_create_view,name='checkout_address_create'),
re_path('^update/$',cart_update,name='cartupdate'),
re_path('^about/$',views.about,name='about' ),
re_path('^login/$',login_page,name='login' ),
re_path('^logout/$',logout_view,name='logout' ),
re_path('^register/$',register_page,name='register' ),
re_path('^checkout/success$',checkout_done,name='success'),
path('products/',include(('products.urls','products'),namespace='products')),
path('search/',include(('search.urls','search'),namespace='search')),
path('my_orders/',include(('orders.urls','orders'),namespace='orders')),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
