from django.urls import path,include,re_path
from .views import SearchListView
from products import views
urlpatterns = [
#path('',views.home,name='searchlist' ),
path('<slug:pk>',views.ProductDetailView.as_view(), name='search_product_detail'),
re_path('^$', SearchListView.as_view(), name='search_list'),
]