from django.shortcuts import render
from django.utils import timezone
from django.views.generic.list import ListView
from products.models import Product
from django.db.models import Q
# Create your views here.

class SearchListView(ListView):
    model = Product
    paginate_by = 10

    template_name = 'products/product_list.html'
    def get_queryset(self):
        query = self.request.GET['query']
        qs=Product.objects.filter(Q(title__icontains=query) |
                  Q(description__icontains=query) |
                  Q(price__icontains=query) )
        return qs

    def get_context_data(self, **kwargs):
        query = self.request.GET['query']
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        context['query']=query
        return context