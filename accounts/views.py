from django.shortcuts import render,HttpResponse,redirect
from django.contrib.auth import get_user,authenticate,login,get_user_model,logout
from .forms import LoginForm,RegisterForm
from django.contrib.auth.decorators import login_required
from django.utils.http import is_safe_url

def login_page(request):
    form=LoginForm(request.POST or None)
    context = {
        "form": form
    }

    # if request.session.get('from_checkout')==True:
    #     context['error']='Login To Checkout'
    #     del request.session['from_checkout']
    #     request.session['tocheckout']=True

    user=get_user(request)
    print(user.is_authenticated)
    next_=request.GET.get('next')
    next_post=request.POST.get('next')
    redirect_path=next_ or next_post
    if form.is_valid():
        print(form.cleaned_data)
        username=form.cleaned_data.get('username')
        password=form.cleaned_data.get('password')
        user=authenticate(request,username=username,password=password)
        print(request.user.is_authenticated)
        if user is not None:
            print(request.user.is_authenticated)
            login(request,user)
            # if request.session.get('tocheckout')==True:
            #     del request.session['tocheckout']
            #     return redirect('cart')
            if is_safe_url(redirect_path,request.get_host()):
                return redirect(redirect_path)
            else:
                # if request.session.get('from_checkout')==True:
                #     return redirect('cart')
                return redirect('/')
            #context['form'] = LoginForm()
            return redirect('/')
        elif user is None:
            context['error']='Invalid credentials'
        else:
            pass
    return render(request, 'accounts/login.html', context)


def register_page(request):
    User = get_user_model()
    form = RegisterForm(request.POST or None)
    context = {
        "form": form,
    }
    if form.is_valid():
        print(form.cleaned_data)
        username = form.cleaned_data.get('username')
        email = form.cleaned_data.get('email')
        password = form.cleaned_data.get('password')
        new_user=User.objects.create_user(username,email,password)
        login(request, new_user)
        return redirect('/')
    return render(request, 'accounts/register.html', context)

@login_required(login_url='/login/')
def logout_view(request):
    logout(request)
    return redirect('/')
