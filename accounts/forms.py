from django import forms
from django.contrib.auth import get_user_model
User=get_user_model()



'''Login Form'''

class LoginForm(forms.Form):
    username=forms.CharField(widget=forms.TextInput(attrs={"class":"form-control","id":"form_username","placeholder":"username"}))
    password=forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','id':'form_password','placehoder':'password'}))

'''Registration Form'''

class RegisterForm(forms.Form):
    username=forms.CharField(widget=forms.TextInput(attrs={"class":"form-control","id":"form_username","placeholder":"username"}))
    email=forms.EmailField(widget=forms.EmailInput(attrs={"class":"form-control","id":"form_full_name","placeholder":"Your email"}))
    password=forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','id':'form_password','placehoder':'password'}))
    confirm_password = forms.CharField(label='Confirm Password', widget=forms.PasswordInput(attrs={'class':'form-control','id':'form_confirm_password','placehoder':'confirm password'}))

    def clean_username(self):
        username = self.cleaned_data.get('username')
        qs=User.objects.filter(username=username)
        if qs.exists():
            raise forms.ValidationError('username already registered')
        return username

    def clean_email(self):
        email = self.cleaned_data.get('email')
        qs=User.objects.filter(email=email)
        if qs.exists():
            raise forms.ValidationError('email already registered')
        return email

    def clean(self):
        data=self.cleaned_data
        password=self.cleaned_data.get('password')
        confirm_password=self.cleaned_data.get('confirm_password')
        if password !=confirm_password:
            raise forms.ValidationError("Passwords must match.")
        return data
