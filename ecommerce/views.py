from django.shortcuts import render,HttpResponse,redirect
from django.contrib.auth import get_user,authenticate,login,get_user_model,logout
# from .forms import ContactForm
from django.contrib.auth.decorators import login_required
# Create your views here.
def home(request):
    context={
        'title':"Welcome To JKart",

        "premium": "Welcome "+request.user.username,
    }
    return render(request,'home.html',context=context)

def about(request):
    context = {
        'title': "About Developer",
    }
    return render(request,'about.html',context=context)

# def contact(request):
#     contact_form=ContactForm(request.POST or None)
#     context = {
#         'title':  "welcome to contact page",
#
#         "form" : contact_form
#     }
#     if contact_form.is_valid():
#         print(contact_form.cleaned_data)
#     return render(request,'home.html',context=context)

