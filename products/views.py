from django.shortcuts import render,Http404
from django.utils import timezone
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from .models import Product
from carts.models import Cart
class ProductListView(ListView):
    model = Product
    paginate_by = 10

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context


class ProductDetailView(DetailView):
    model = Product

    def get_context_data(self, **kwargs):
        request=self.request
        context = super().get_context_data(**kwargs)
        cart_obj, new_obj = Cart.objects.new_or_get(request)
        context['cart']=cart_obj
        context['now'] = timezone.now()
        return context