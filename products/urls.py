from django.urls import path,re_path

from . import views

urlpatterns = [
    re_path('^$', views.ProductListView.as_view(), name='productlist'),
    path('<slug:pk>/', views.ProductDetailView.as_view(), name='productdetail')
]