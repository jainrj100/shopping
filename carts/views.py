from django.shortcuts import render,redirect,reverse
from .models import Cart
from billing.models import BillingProfile
from products.models import Product
from .models import Cart
from orders.models import Order
from accounts.forms import LoginForm
from addresses.forms import AddressForm
from addresses.models import Address
from django.contrib.auth.decorators import login_required

def cart_home(request):
    cart_obj,new_obj=Cart.objects.new_or_get(request)
    return render(request,'carts/home.html',{"cart":cart_obj})

def cart_update(request):
    product_id=request.POST.get('product_id')
    if product_id is not None:
        try:
            product_obj=Product.objects.get(id=product_id)
        except Product.DoesNotExist:
            print('product is gone')
            return redirect('cart')
        product_obj=Product.objects.get(id=product_id)
        cart_obj, new_obj = Cart.objects.new_or_get(request)
        if product_obj in cart_obj.products.all():
            cart_obj.products.remove(product_obj)
        else:
             cart_obj.products.add(product_obj)
        request.session['cart_items']=cart_obj.products.count()
    return redirect('cart')

@login_required(login_url='/login/')
def checkout_home(request):
    # if not request.user.is_authenticated:
    #     request.session['from_checkout']=True
    #     return redirect('login')
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    order_obj=None
    if cart_obj.products.count()==0:
        return redirect('cart')
    login_form=LoginForm()
    address_form=AddressForm()
    billing_address_id=request.session.get('billing_address_id',None)
    shipping_address_id=request.session.get('shipping_address_id',None)
    billing_profile,billing_profile_created=BillingProfile.objects.new_or_get(request)
    if billing_profile is not None:
        order_qs=Order.objects.filter(billing_profile=billing_profile,cart=cart_obj,active=True,status='created')
        if order_qs.count()==1:
            order_obj=order_qs.first()
        else:
            old_order_qs=Order.objects.exclude(billing_profile=billing_profile).filter(cart=cart_obj,active=True)
            if old_order_qs.exists():
                old_order_qs.update(active=False)
            order_obj=Order.objects.create(billing_profile=billing_profile,cart=cart_obj)
        if shipping_address_id:
            order_obj.shipping_address=Address.objects.get(id=shipping_address_id)
            del request.session['shipping_address_id']
        if billing_address_id:
            order_obj.billing_address=Address.objects.get(id=billing_address_id)
            del request.session['billing_address_id']
        if shipping_address_id or billing_address_id:
            order_obj.save()
    if request.method=="POST":
        is_done=order_obj.check_done()
        if is_done:
            order_obj.mark_paid()
            del request.session['cart_id']
            request.session['cart_items']=0
            return redirect('success')
    context={
        "object": order_obj,
        "billing_profile":billing_profile,
        'login_form':login_form,
        'address_form':address_form,
    }
    return render(request,'carts/checkout.html',context)

def checkout_done(request):
    return render(request,'carts/checkout-done.html',{})

