from django.urls import path,re_path

from . import views

urlpatterns = [
    re_path('^$', views.myorders, name='myorders'),
    path('<slug:pk>/', views.OrderDetailView.as_view(), name='orderdetail')

]