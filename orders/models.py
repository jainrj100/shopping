from django.db import models
from carts.models import Cart
import uuid
from django.db.models.signals import pre_save,post_save
import math
from billing.models import BillingProfile
from addresses.models import Address
from django.utils import timezone


ORDER_STATUS_CHOICES=(
    ('created','Created'),
('paid','Paid'),
('shipped','Shipped'),
('refunded','Refunded'),
)

class Order(models.Model):
    billing_profile=models.ForeignKey(BillingProfile,on_delete=models.CASCADE,null=True,blank=True)
    order_id=models.CharField(max_length=200,blank=True)
    shipping_address=models.ForeignKey(Address,related_name='shipping_address',on_delete=models.CASCADE,null=True,blank=True)
    billing_address=models.ForeignKey(Address,related_name='billing_address',on_delete=models.CASCADE,null=True,blank=True)
    cart=models.ForeignKey(Cart,on_delete=models.CASCADE)
    status=models.CharField(max_length=120,default='created',choices=ORDER_STATUS_CHOICES)
    shipping_total=models.DecimalField(max_digits=100,decimal_places=2,default=5.99)
    total=models.DecimalField(max_digits=100,decimal_places=2,default=0.00)
    active=models.BooleanField(default=True)
    paid_at = models.DateField(blank=True,null=True)

    def __str__(self):
        return self.order_id

    def update_total(self):
        cart_total=self.cart.total
        shipping_total=self.shipping_total
        newtotal= math.fsum([cart_total,shipping_total])
        formatted_total=format(newtotal,'0.2f')
        self.total=formatted_total
        self.save()
        return newtotal

    def check_done(self):
        billing_profile=self.billing_profile
        shipping_address=self.shipping_address
        billing_address=self.billing_address
        total=self.total
        if billing_profile and shipping_address and billing_address and total>0 :
            return True
        else:
            return False

    def mark_paid(self):
        if self.check_done():
            self.status='paid'
            self.paid_at=timezone.now()
            self.save()
        return self.status

def pre_save_create_order_id(sender,instance,*args,**kwargs):
    if not instance.order_id:
        instance.order_id=uuid.uuid4().hex[:15].upper()
        qs=Order.objects.filter(cart=instance.cart).exclude(billing_profile=instance.billing_profile)
        if qs.exists():
            qs.update(active=False)

pre_save.connect(pre_save_create_order_id,sender=Order)

def post_save_cart_total(sender,instance,created,*args,**kwargs):
    if not created:
        cart_obj=instance
        cart_total=cart_obj.total
        cart_id=cart_obj.id
        qs=Order.objects.filter(cart__id=cart_id)
        if qs.count()==1:
            order_obj=qs.first()
            order_obj.update_total()

post_save.connect(post_save_cart_total,sender=Cart)

def post_save_order(sender,instance,created,*args,**kwargs):
    if created:
        instance.update_total()

post_save.connect(post_save_order,sender=Order)
