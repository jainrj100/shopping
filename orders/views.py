from django.shortcuts import render,HttpResponse
from .models import Order
from django.contrib.auth.decorators import login_required
from billing.models import BillingProfile
from django.views.generic.detail import DetailView






@login_required(login_url='/login/')
def myorders(request):
    user=request.user
    billingprofile=BillingProfile.objects.get(user=user)
    user_order=Order.objects.filter(billing_profile=billingprofile,status='paid')
    return render(request,'orders/myorders.html',{'user_orders':user_order})


class OrderDetailView(DetailView):
    model = Order
    template_name = 'orders/order_details.html'
    def get_context_data(self, **kwargs):
        request=self.request
        context = super().get_context_data(**kwargs)

        return context