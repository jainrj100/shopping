from django.db import models
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
User=get_user_model()


class BillingProfileManager(models.Manager):
    def new_or_get(self,request):
        user=request.user
        billing_profile_created=False
        billing_profile=None
        print('billing used')
        # entry=self.model.objects.filter(user=user)
        # if entry.exists() and entry.count()==1:
        #     entry.first().email=user.email
        #     entry.first().save()
        #     billing_profile,billing_profile_created=entry.first(),False
        # else:
        billing_profile, billing_profile_created = self.model.objects.get_or_create(user=user, email=user.email)
        return billing_profile,billing_profile_created



class BillingProfile(models.Model):
    user=models.OneToOneField(User,on_delete=models.CASCADE)
    email=models.EmailField(blank=False)
    active=models.BooleanField(default=True)
    update=models.DateTimeField(auto_now=True)
    timestamp=models.DateTimeField(auto_now_add=True)
    objects=BillingProfileManager()
    def __str__(self):
        return self.email

# def user_created_reciever(sender,instance,created,*args,**kwargs):
#     if created:
#         BillingProfile.objects.get_or_create(user=instance)
#
# post_save.connect(user_created_reciever,sender=User)